const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    context: path.resolve(__dirname, '..'),
    name: 'development',
    entry: {
        app: './app/index.jsx',
    },
    output: {
        path: path.resolve(__dirname, '..', 'build'),
        filename: 'js/bundle.js',
        publicPath: '/'
    },
    resolve: {
        modules: ['node_modules', path.resolve(__dirname, '..', 'app')],
        extensions: ['.js', '.jsx', '.css']
    },
    plugins: [
        new HtmlWebpackPlugin({
            filename: 'index.html',
            template: 'app/static/index.html',
            inject: 'body'
        })
    ],
    module: {
        rules: [
            {
                test: /\.(png|jpg|ttf|woff|woff2)$/,
                use: [
                    {
                        loader: 'url-loader',
                        options: {
                            name: 'img/[name].[ext]',
                            limit: 10000,
                        },
                    },
                ],
            },
            {
                test: /\.(styl|css)$/,
                use: [
                    'style-loader',
                    'css-loader',
                    'stylus-loader',
                ],
            },
            {
                test:  /\.html$/,
                use: 'html-loader'
            },
            {
                test: /\.jsx?$/,
                use: [
                    {
                        loader: 'babel-loader',
                        options: {
                            presets: ['es2015', 'react', 'stage-0'],
                            plugins: [
                                'transform-decorators-legacy',
                                'transform-runtime',
                                'transform-regenerator',
                                'transform-async-to-generator',
                            ],
                        },
                    },
                ],
                exclude: /node_modules/,
            },
        ]
    },
    devServer: {
        host: '0.0.0.0',
        port: 8001,
        contentBase: './build',
        noInfo: false,
        inline: true,
        historyApiFallback: {
            index: '/'
        }
    }
};