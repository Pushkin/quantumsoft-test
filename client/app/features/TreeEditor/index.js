import { reducer } from './redux';
import TreeEditor from './view/TreeEditor';

export default TreeEditor;
export { reducer };