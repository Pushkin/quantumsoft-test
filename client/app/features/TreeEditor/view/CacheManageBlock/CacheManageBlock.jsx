import React from 'react';
import PropTypes from 'prop-types';
import block from 'bem-cn';
import { Button } from 'react-bootstrap'

import './CacheManageBlock.styl'

function CacheManageBlock(props) {
    const { onAddItemClick, onDeleteItemClick, onEditItemClick } = props;
    const b = block('manage-block');
    return (
        <div className={b()}>
            <Button bsStyle="success" onClick={onAddItemClick}>
                <span className={b('button-text')}>Add</span>
            </Button>
            <Button bsStyle="danger"  onClick={onDeleteItemClick}>
                <span className={b('button-text')}>Delete</span>
            </Button>
            <Button bsStyle="info"  onClick={onEditItemClick}>
                <span className={b('button-text')}>Edit</span>
            </Button>
        </div>
    )
}

CacheManageBlock.PropTypes = {
    onAddItemClick: PropTypes.func,
    onDeleteItemClick: PropTypes.func,
    onEditItemClick: PropTypes.func,
}

CacheManageBlock.defaultProps = {
    onAddItemClick: () => {},
    onDeleteItemClick: () => {},
    onEditItemClick: () => {},
}

export default CacheManageBlock;
