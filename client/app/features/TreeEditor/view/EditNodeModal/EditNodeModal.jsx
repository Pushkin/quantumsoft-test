import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bind } from 'decko';
import { Modal, FormGroup, FormControl, ControlLabel, Button } from 'react-bootstrap';

import './EditNodeModal.styl';

class EditNodeModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            inputValue: '',
        }
    }

    static propTypes = {
        show: PropTypes.bool.isRequired,
        onHide: PropTypes.func.isRequired,
        onNewValueEntered: PropTypes.func.isRequired,
        initialValue: PropTypes.string,
    };

    static defaultProps = {
        initialValue: '',
    };

    @bind
    onSaveValueClick() {
        const { onNewValueEntered } = this.props;
        const { inputValue } = this.state;

        onNewValueEntered(inputValue);
    }

    @bind
    handleChange(e) {
        this.setState({ inputValue: e.target.value });
    }

    @bind
    getValidationState() {
        if (this.state.inputValue.length === 0)
            return 'error';

        return 'success'
    }

    componentWillReceiveProps(nextProps) {
        this.setState({ inputValue: nextProps.initialValue });
    }

    render() {
        const { show, onHide } = this.props;
        return (
            <Modal show={show} onHide={onHide}>
                <Modal.Header closeButton>
                    <Modal.Title>Edit node value</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <FormGroup validationState={this.getValidationState()}>
                      <ControlLabel>Enter a new node value:</ControlLabel>
                      <FormControl
                        type="text"
                        value={this.state.inputValue}
                        placeholder="Cannot be empty"
                        onChange={this.handleChange}
                      />
                      <FormControl.Feedback />
                    </FormGroup>
                    <Button
                        bsStyle="success"
                        disabled={this.getValidationState() !== 'success'}
                        onClick={this.onSaveValueClick}
                    >
                        Save
                    </Button>
                </Modal.Body>
            </Modal>
        )
    }
}

export default EditNodeModal;