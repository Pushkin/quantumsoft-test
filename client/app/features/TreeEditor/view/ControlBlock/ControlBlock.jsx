import React from 'react';
import block from 'bem-cn';
import PropTypes from 'prop-types';
import { Button } from 'react-bootstrap';

import './ControlBlock.styl';

function ControlBlock(props) {
    const b = block('control-block');
    const { onExtractClick, onApplyClick, onResetClick, onRemoveClick } = props;
    return (
        <div className={b()}>
            <Button bsStyle="info" onClick={onExtractClick}>
                { '<< Extract node' }
            </Button>
            <Button bsStyle="danger" onClick={onRemoveClick}>
                { 'Remove node from cache >>' }
            </Button>
            <Button bsStyle="success" onClick={onApplyClick}>
                { 'Apply changes >>>' }
            </Button>
            <Button bsStyle="warning" onClick={onResetClick}>
                { 'Reset application' }
            </Button>
        </div>
    );
}

ControlBlock.propTypes = {
    onExtractClick: PropTypes.func,
    onRemoveClick: PropTypes.func,
    onApplyClick: PropTypes.func,
    onResetClick: PropTypes.func,
};

export default ControlBlock;
