import React, { Component } from 'react';
import PropTypes from "prop-types";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { bind } from 'decko';
import block from 'bem-cn';
import { actions as treeEditorActions } from '../../redux';

import NotificationBlock, { messageTypes } from 'shared/views/NotificationBlock';
import ControlBlock from '../ControlBlock';
import CacheManageBlock from '../CacheManageBlock';
import EditNodeModal from '../EditNodeModal';

import './TreeEditor.styl';

import TreeView from 'shared/views/TreeView';

const initialState = {
    dbViewSelectedItem: null,
    cacheViewSelectedItem: null,
    showEditModal: false,
};

class TreeEditor extends Component {
    constructor(props) {
        super(props);
        this.state = initialState;
    }

    static propTypes = {
        dbNodes: PropTypes.arrayOf(PropTypes.object).isRequired,
        cachedNodes: PropTypes.arrayOf(PropTypes.object).isRequired,

        hideNotifications: PropTypes.func,
        warningMessage: PropTypes.string,
        errorMessage: PropTypes.string,

        removeNodeFromCache: PropTypes.func.isRequired,
        removeHangingNodes: PropTypes.func.isRequired,
        applyTreeChanges: PropTypes.func.isRequired,
        loadWholeTree: PropTypes.func.isRequired,
        loadItemById: PropTypes.func.isRequired,
        createNode: PropTypes.func.isRequired,
        deleteNode: PropTypes.func.isRequired,
        resetTree: PropTypes.func.isRequired,
        editNode: PropTypes.func.isRequired,
    };

    static defaultProps = {
        warningMessage: null,
        errorMessage: null,
        hideNotifications: () => {},
    }

    componentWillReceiveProps(nextProps) {
        const { warningMessage, errorMessage, hideNotifications } = nextProps;
        // Checking if we just received a new warning or error
        if ( this.props.warningMessage === null && warningMessage !== null ||
                this.props.ErrorMessage === null && errorMessage !== null
        ) {
            // Make this message hide after 3 seconds
            setTimeout(() => { hideNotifications() }, 3000);
        }
    }

    componentDidMount() {
        this.props.loadWholeTree()
    }

    @bind
    onSelectDbViewActiveItem(node) {
        this.setState({ dbViewSelectedItem: node })
    }

    @bind
    onSelectCacheViewActiveItem(node) {
        this.setState({ cacheViewSelectedItem: node })
    }

    @bind
    onRemoveNodeClickHandler() {
        const { cacheViewSelectedItem } = this.state;
        const { removeNodeFromCache } = this.props;
        removeNodeFromCache(cacheViewSelectedItem)
            .then(this.setState({ cacheViewSelectedItem: null }));
    }

    @bind
    onExtractNodeClickHandler() {
        const { loadNodeById } = this.props;
        const { dbViewSelectedItem } = this.state;

        if (dbViewSelectedItem !== null)
            loadNodeById(dbViewSelectedItem.id);
    }

    @bind
    onApplyChangesClickHandler() {
        const { applyTreeChanges, loadWholeTree, cachedNodes, removeHangingNodes } = this.props;
        applyTreeChanges(cachedNodes)
            .then(loadWholeTree)
            .then(removeHangingNodes)
            .then(() => this.setState(initialState))
    }

    @bind
    onResetAppClickHandler() {
        this.props.resetTree();
    }

    @bind
    onAddNewCacheItemHandler() {
        const { createNode } = this.props;
        const { cacheViewSelectedItem } = this.state;
        createNode(cacheViewSelectedItem);
    }

    @bind
    onDeleteCacheItemHandler() {
        const { cacheViewSelectedItem } = this.state;
        const { deleteNode } = this.props;

        deleteNode(cacheViewSelectedItem).then(() => this.setState({ cacheViewSelectedItem: null }));
    }

    @bind
    onEditCacheItemHandler() {
        const { cacheViewSelectedItem } = this.state;
        if (cacheViewSelectedItem !== null && cacheViewSelectedItem.is_deleted === false)
            this.setState({ showEditModal: true });
    }

    @bind
    onModalHide(){
        this.setState({ showEditModal: false });
    }

    @bind
    onNodeNewValueEntered(newValue) {
        this.setState({ showEditModal: false });
        const { editNode } = this.props;
        const { cacheViewSelectedItem } = this.state;
        editNode(cacheViewSelectedItem, newValue);
    }

    render() {
        const b = block('tree-editor');
        const { dbViewSelectedItem, cacheViewSelectedItem, showEditModal } = this.state;
        const { dbNodes, cachedNodes, warningMessage, errorMessage } = this.props;
        return (
            <div className={b()}>
                <EditNodeModal
                    show={showEditModal}
                    onHide={this.onModalHide}
                    onNewValueEntered={this.onNodeNewValueEntered}
                    initialValue={cacheViewSelectedItem === null ? '' : cacheViewSelectedItem.title}
                />
                <h3 className={b('header')}>TREE EDITOR</h3>
                <div className={b('tree-view-container')}>
                    <div>
                        <TreeView
                            header='Cache View'
                            nodes={cachedNodes}
                            onSelectActiveElement={this.onSelectCacheViewActiveItem}
                            selectedItem={cacheViewSelectedItem}
                        />
                        <CacheManageBlock
                            onAddItemClick={this.onAddNewCacheItemHandler}
                            onDeleteItemClick={this.onDeleteCacheItemHandler}
                            onEditItemClick={this.onEditCacheItemHandler}
                        />
                    </div>
                    <ControlBlock
                        onExtractClick={this.onExtractNodeClickHandler}
                        onRemoveClick={this.onRemoveNodeClickHandler}
                        onApplyClick={this.onApplyChangesClickHandler}
                        onResetClick={this.onResetAppClickHandler}
                    />
                    <TreeView
                        header='DB View'
                        nodes={dbNodes}
                        onSelectActiveElement={this.onSelectDbViewActiveItem}
                        selectedItem={dbViewSelectedItem}
                    />
                </div>
                { warningMessage && <NotificationBlock type={messageTypes.WARNING} message={warningMessage}/> }
                { errorMessage && <NotificationBlock message={errorMessage}/> }
            </div>
        );
    }
}

function mapStateToProps(state) {
  return {
      dbNodes: state.treeEditor.dbNodes,
      cachedNodes: state.treeEditor.cachedNodes,
      warningMessage: state.treeEditor.warningMessage,
      errorMessage: state.treeEditor.errorMessage,
  };
}

function mapDispatchToProps(dispatch) {
  const actions = {
      loadWholeTree: treeEditorActions.loadWholeTree,
      loadNodeById: treeEditorActions.loadNodeById,
      hideNotifications: treeEditorActions.hideNotifications,
      createNode: treeEditorActions.createNode,
      resetTree: treeEditorActions.resetTree,
      applyTreeChanges: treeEditorActions.applyTreeChanges,
      deleteNode: treeEditorActions.deleteNode,
      removeHangingNodes: treeEditorActions.removeHangingNodes,
      editNode: treeEditorActions.editNode,
      removeNodeFromCache: treeEditorActions.removeNodeFromCache,
  };
  return bindActionCreators(actions, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(TreeEditor);
