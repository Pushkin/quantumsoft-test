import { fromJS, List, Map } from 'immutable';
import initialState from './initialData';
import { actionTypes } from "./actions";

function reducer(state = initialState, action) {
    const imState = fromJS(state);
    let imCachedNodes = imState.getIn(['cachedNodes']);

    switch (action.type) {
        case actionTypes.CREATE_NODE:
            return imState
                .setIn(['cachedNodes'], imCachedNodes.push(action.payload))
                .toJS();

        case actionTypes.LOAD_WHOLE_TREE_SUCCESS:
            return imState
                .setIn(['dbNodes'], action.payload)
                .toJS();

        case actionTypes.LOAD_NODE_BY_ID_SUCCESS:
            return imState
                .setIn(['cachedNodes'], imCachedNodes.push(action.payload))
                .toJS();

        case actionTypes.RESET_TREE_SUCCESS:
            return fromJS(initialState)
                .setIn(['dbNodes'], action.payload)
                .toJS();

        case actionTypes.REMOVE_HANGING_NODES:
            return imState
                .setIn(
                    ['cachedNodes'],
                    imCachedNodes.filter(node => node.getIn(['is_deleted']) === false)
                )
                .toJS();

        case actionTypes.REMOVE_NODE_FROM_CACHE:
            return imState
                .setIn(
                    ['cachedNodes'],
                    imCachedNodes.delete(imCachedNodes.findIndex(node => node.getIn(['id']) === action.payload.id))
                )
                .toJS();

        case actionTypes.EDIT_NODE:
            return imState
                .setIn(
                    ['cachedNodes'],
                    imCachedNodes.update(
                        imCachedNodes.findIndex(node => node.getIn(['id']) === action.payload.node.id),
                        node => node.setIn(['title'], action.payload.value),
                    )
                )
                .toJS();

        case actionTypes.DELETE_NODE:
            return imState
                .setIn(['cachedNodes'], imCachedNodes.map((node) => {
                    if (node.getIn(['path']).includes(action.payload.path))
                        return node.setIn(['is_deleted'], true);

                    return node
                }))
                .toJS();

        case actionTypes.APPLY_CHANGES_SUCCESS:
            const nodes_assignments = action.payload.new_nodes_assignments;

            // updating nodes with new id and path assignments from server
            for (const key of Object.keys(nodes_assignments)) {
                const index = imCachedNodes.findIndex(node => node.getIn(['id']) == key);

                if ( index < 0 )
                    console.error('Error in matching new nodes assignments with cache');
                else
                    imCachedNodes = imCachedNodes.setIn([index], new Map(nodes_assignments[key]));
            }

            return imState
                .setIn(['cachedNodes'], imCachedNodes)
                .toJS();

        case actionTypes.ACTION_WARNING:
            return imState
                .setIn(['warningMessage'], action.payload)
                .toJS();

        case actionTypes.REQUEST_FAILURE:
            return imState
                .setIn(['errorMessage'], action.payload)
                .toJS();

        case actionTypes.HIDE_NOTIFICATIONS:
            return imState
                .setIn(['warningMessage'], null)
                .setIn(['errorMessage'], null)
                .toJS();

        default:
            return imState.toJS()
    }
}

export default reducer;
