const initialState = {
    cachedNodes: [],
    dbNodes: [],
    warningMessage: null,
    errorMessage: null,
}

export default initialState;
