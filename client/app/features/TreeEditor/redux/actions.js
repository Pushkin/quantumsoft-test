const actionTypes = {
    // setting some unique strings as values
    LOAD_WHOLE_TREE_SUCCESS: 'client/treeEditor/LOAD_WHOLE_TREE_SUCCESS',
    LOAD_NODE_BY_ID_SUCCESS: 'client/treeEditor/LOAD_NODE_BY_ID',
    RESET_TREE_SUCCESS: 'client/treeEditor/RESET_NODES_SUCCESS',
    APPLY_CHANGES_SUCCESS: 'client/treeEditor/APPLY_CHANGES_SUCCESS',
    REQUEST_FAILURE: 'client/treeEditor/REQUEST_FAILURE',

    ACTION_WARNING: 'client/treeEditor/ACTION_WARNING',
    HIDE_NOTIFICATIONS: 'client/treeEditor/HIDE_NOTIFICATIONS',
    REMOVE_HANGING_NODES: 'client/treeEditor/REMOVE_HANGING_NODES',

    REMOVE_NODE_FROM_CACHE: 'client/treeEditor/REMOVE_NODE_FROM_CACHE',

    CREATE_NODE: 'client/treeEditor/CREATE_NODE',
    DELETE_NODE: 'client/treeEditor/DELETE_NODE',
    EDIT_NODE: 'client/treeEditor/EDIT_NODE',
};

function removeNodeFromCache(node) {
    return async dispatch => dispatch({ type: actionTypes.REMOVE_NODE_FROM_CACHE, payload: node });
}

function removeHangingNodes() {
    return async dispatch => dispatch({ type: actionTypes.REMOVE_HANGING_NODES })
}

function editNode(node, nodeNewValue) {
    return async dispatch => dispatch({
        type: actionTypes.EDIT_NODE,
        payload: {
            node: node,
            value: nodeNewValue,
        }
    })
}

function deleteNode(node) {
    return async (dispatch) => {
        if (node === null) {
            dispatch({ type: actionTypes.ACTION_WARNING, payload: 'Select an item to remove' });
            return;
        }
        dispatch({ type: actionTypes.DELETE_NODE, payload: node});
    }
}

function applyTreeChanges(cachedNodes) {
    return async (dispatch, getState, extra) => {
        const { api } = extra;
        const response = await api.applyTreeChanges(cachedNodes);

        if (response.success) {
            dispatch({ type: actionTypes.APPLY_CHANGES_SUCCESS, payload: response.data });
        } else {
            dispatch({ type: actionTypes.REQUEST_FAILURE, payload: response.errorMessage });
        }
    }
}

function resetTree() {
    return async (dispatch, getState, extra) => {
        const { api } = extra;
        const response = await api.resetTree();

        if (response.success) {
            dispatch({ type: actionTypes.RESET_TREE_SUCCESS, payload: response.data });
        } else {
            dispatch({ type: actionTypes.REQUEST_FAILURE, payload: response.errorMessage });
        }
    };
}

function createNode(parentNode) {
    return async (dispatch, getState) => {
        if (parentNode === null) {
            dispatch({ type: actionTypes.ACTION_WARNING, payload: 'Select a node which to append child to' });
            return;
        }

        const cachedNodes = getState().treeEditor.cachedNodes;
        const minId = Math.min(...cachedNodes.map(item => item.id));
        // new elements added to the cache are started from id = -1 and lower
        const newId = minId < 0 ? minId - 1 : -1;
        const newNode = {
            id: newId,
            path: `${parentNode.path}.${newId}`,
            title: `NEW NODE${newId}`,
            is_deleted: false,
        };

        dispatch({ type: actionTypes.CREATE_NODE, payload: newNode });
    };
}

function hideNotifications() {
    return async (dispatch) => dispatch({ type: actionTypes.HIDE_NOTIFICATIONS })
}

function loadWholeTree() {
    return async (dispatch, getState, extra) => {
        const { api } = extra;
        const response = await api.loadWholeTree();

        if (response.success) {
            dispatch({ type: actionTypes.LOAD_WHOLE_TREE_SUCCESS, payload: response.data });
        } else {
            dispatch({ type: actionTypes.REQUEST_FAILURE, payload: response.errorMessage });
        }
    };
}

function loadNodeById(nodeId) {
    return async (dispatch, getState, extra) => {
        // Checking if such node is already loaded
        if (getState().treeEditor.cachedNodes.findIndex((item) => item.id === nodeId) !== -1) {
            dispatch({type: actionTypes.ACTION_WARNING, payload: 'Such element already exists in the cache'});
            return;
        }

        const { api } = extra;
        const response = await api.loadNodeById(nodeId);

        // Detecting if extracted node is gonna be deleted
        if (response.success) {
            const node = response.data;
            for (const cachedNode of getState().treeEditor.cachedNodes)
                if (cachedNode.is_deleted && node.path.includes(cachedNode.path))
                    node.is_deleted = true;

            dispatch({ type: actionTypes.LOAD_NODE_BY_ID_SUCCESS, payload: node });
        } else {
            dispatch({ type: actionTypes.REQUEST_FAILURE, payload: response.errorMessage });
        }
    };
}

export {
    actionTypes,
    loadWholeTree,
    loadNodeById,
    hideNotifications,
    createNode,
    resetTree,
    applyTreeChanges,
    deleteNode,
    removeHangingNodes,
    editNode,
    removeNodeFromCache,
};
