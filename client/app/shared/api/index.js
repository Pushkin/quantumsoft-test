import axios from 'axios';
import { bind } from 'decko';


const API_URLS = {
    URL_RESET_TREE: 'reset/',
    URL_WHOLE_TREE: 'data_node/all/',
    URL_GET_NODE_BY_ID: 'data_node/',
    URL_APPLY_NODE_CHANGES: 'apply_node_changes/',
};


class TreeApi {
    constructor(baseURL = '') {
        const config = {
            baseURL,
            withCredentials: false,
            validateStatus: status => status < 550,
        };

        this.apiClient = axios.create(config);
    }

    @bind
    async applyTreeChanges(cachedNodes) {
        const response = await this._post(API_URLS.URL_APPLY_NODE_CHANGES, cachedNodes);
        return this._processResponse(response)
    }

    @bind
    async resetTree() {
        const response = await this._get(API_URLS.URL_RESET_TREE);
        return this._processResponse(response)
    }

    @bind
    async loadWholeTree() {
        const response = await this._get(API_URLS.URL_WHOLE_TREE);
        return this._processResponse(response)
    }

    @bind
    async loadNodeById(itemId) {
        const response = await this._get(`${API_URLS.URL_GET_NODE_BY_ID}${itemId}/`);
        return this._processResponse(response)
    }

    @bind
    _get(url, params) {
        return this.apiClient.get(url, params);
    }

    @bind
    _post(url, data) {
        return this.apiClient.post(url, data);
    }

    @bind
    _processResponse(response) {
        const success = response.status === 200;
        const responseData = JSON.parse(response.data);

        return {
            success: success,
            data: success ? responseData: null,
            errorMessage: success ? null : responseData.error_message,
        };
    }
}

export default TreeApi;
