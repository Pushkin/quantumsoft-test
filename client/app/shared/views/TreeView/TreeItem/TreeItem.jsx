import React, { Component } from 'react';
import PropTypes from 'prop-types';
import block from 'bem-cn';
import { bind } from 'decko';

import './TreeItem.styl';

const LEVEL_MARGIN = 25;

class TreeItem extends Component {
    static propTypes = {
        node: PropTypes.shape({
            id: PropTypes.number,
            title: PropTypes.string,
            path: PropTypes.string,
            is_deleted: PropTypes.bool,
            indentationLevel: PropTypes.number,
        }).isRequired,
        isSelected: PropTypes.bool,
        onSelectItem: PropTypes.func,
    }

    static defaultProps = {
        isSelected: false,
        onSelectItem: (node) => {},
    }

    @bind
    onClickHandler() {
        const { node, onSelectItem } = this.props;
        onSelectItem(node);
    }

    render() {
        const b = block('tree-item');
        const { node, isSelected } = this.props;

        return (
            <div
                className={b({ selected: isSelected, deleted: node.is_deleted })}
                style={ { marginLeft: `${ LEVEL_MARGIN * node.indentationLevel }px` } }
                onClick={this.onClickHandler}
            >
                { node.title }
            </div>
        );
    }
}

export default TreeItem;
