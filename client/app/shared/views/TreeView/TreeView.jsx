import React, { Component } from 'react';
import PropTypes from 'prop-types';
import block from 'bem-cn';
import { bind } from 'decko';

import TreeItem from './TreeItem';

import './TreeView.styl';

class TreeView extends Component {
    static propTypes = {
        header: PropTypes.string,
        nodes: PropTypes.arrayOf(PropTypes.shape({
            id: PropTypes.number,
            title: PropTypes.string,
            path: PropTypes.string,
            is_deleted: PropTypes.bool,
        })),
        selectedItem: PropTypes.shape({
            id: PropTypes.number,
            title: PropTypes.string,
            path: PropTypes.string,
            is_deleted: PropTypes.bool,
            indentationLevel: PropTypes.number,
        }),
        onSelectActiveElement: PropTypes.func,
    };

    static defaultProps = {
        nodes: [],
        onSelectActiveElement: (node) => {},
    };

    organizeNodes(nodes) {
        // Constructing nodes map separating elements by level
        const nodesMap = {};
        let minLevel = Number.MAX_VALUE;
        for (const node of nodes){
            const level = node.path.split('.').length - 1;

            if (level < minLevel) minLevel = level;

            if (nodesMap[level] === undefined)
                nodesMap[level] = [node];
            else
                nodesMap[level].push(node);
        }

        // Calculating indentation levels for each element and constructing the result
        let result = [];
        let currentLevel = minLevel;

        while (result.length < nodes.length) {
            if (currentLevel === minLevel)
                result.push( ...nodesMap[currentLevel].map((node) => ({ ...node, indentationLevel: 0 })));
            else if (nodesMap[currentLevel] !== undefined)
                for (const node of nodesMap[currentLevel]) {
                    const parentNode = this.getDirectParentNode(node, result);
                    if (parentNode !== null) {
                        this.insertNodeAfterId(parentNode.id, { ...node, indentationLevel: parentNode.indentationLevel + 1 }, result);
                    }
                    else result.push({ ...node, indentationLevel: 0 });
                }

            currentLevel ++;
        }

        return result;
    }

    getDirectParentNode(currentNode, nodesArray) {
        let parentPath = currentNode.path.split('.');
        parentPath = [...parentPath.slice(0, parentPath.length-1)].join('.');

        for (const node of nodesArray)
            if (node.path === parentPath) return node;

        return null;
    }

    insertNodeAfterId(nodeId, newNode, nodesArray) {
        let index = nodesArray.findIndex((node) => node.id === nodeId) + 1;
        nodesArray.splice(index, 0, newNode);
    }

    @bind
    onSelectItemHandler(node) {
        if (node.is_deleted === false)
            this.props.onSelectActiveElement(node);
    }

    render() {
        const b = block('tree-view');

        let { nodes, selectedItem, header } = this.props;

        nodes = this.organizeNodes(nodes);
        const node_elements = nodes.map((node) => {
            return (
                <TreeItem
                    key={node.id}
                    node={node}
                    isSelected={selectedItem !== null && selectedItem.id === node.id}
                    onSelectItem={this.onSelectItemHandler}
                />
            );
        });
        return (
            <div className={b()}>
                <h3 className={b('header')}>{ header }</h3>
                <div className={b('container')}>
                    {node_elements}
                </div>
            </div>
        );
    }
}

export default TreeView;
