import React from 'react';
import PropTypes from 'prop-types';
import block from 'bem-cn';

import './NotificationBlock.styl';

const messageTypes = {
    ERROR: 'error',
    WARNING: 'warning',
}

function NotificationBlock(props) {
    const { type, message } = props;
    const b = block('notification-block');
    return (
      <div className={b()}>
          <span className={b(type)}>{ message }</span>
      </div>
    );
}

NotificationBlock.propTypes = {
    message: PropTypes.string.isRequired,
    type: PropTypes.string,
};

NotificationBlock.defaultProps = {
    type: messageTypes.ERROR,
};

export default NotificationBlock;
export {
    messageTypes,
}
