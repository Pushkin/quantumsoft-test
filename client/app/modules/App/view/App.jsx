import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './App.styl';

import TreeEditor from 'features/TreeEditor';

class App extends Component {
    render() {
        return (
            <div>
                <TreeEditor />
                <div>
                    { this.props.children }
                </div>
            </div>
        );
    }
}

export default App;

