import { Route } from 'react-router';
import React from 'react'
// import { TourneysContainer } from 'modules/containers/TourneysContainer';
import App from './modules/App';

function createRoutes() {
    return (
        <Route path="/" component={App} />
    );
}

export default createRoutes;
