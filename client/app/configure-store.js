import {
    createStore,
    combineReducers,
    applyMiddleware,
    compose
} from 'redux';
import thunk from 'redux-thunk';

import { reducer as treeEditorReducer} from 'features/TreeEditor';
// import line from 'modules/redux/line';
// import sports from 'modules/redux/sports';
// import tourneys from 'modules/redux/tourneys';
// import auth from 'modules/redux/auth';
// import basket from 'modules/redux/basket';

function configureStore(extra) {
    const middlewares = [
        thunk.withExtraArgument(extra)
    ];

    const reducer = combineReducers({
        treeEditor: treeEditorReducer,
        // line,
        // sports,
        // tourneys,
        // auth,
        // basket
    });

    return createStore(
        reducer,
        compose(
            applyMiddleware(...middlewares),
            window.devToolsExtension ? window.devToolsExtension() : f => f
        ),
    )
}

export default configureStore;
