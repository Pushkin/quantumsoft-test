import { render } from 'react-dom';
import { Provider } from 'react-redux';
import React from 'react';
// import 'bootstrap/dist/css/bootstrap.min.css';
// import 'babel-polyfill';
import { Router, browserHistory } from 'react-router';
import configureStore from './configure-store';
import createRoutes from './create-routes';
import TreeApi from './shared/api'
// import './bootstrap/bootstrap.css';
// import './opensans/opensans.css';

const BASE_URL = 'http://localhost:8000/api/';
const api = new TreeApi(BASE_URL);

const store = configureStore({ api });
const routes = createRoutes();
const root = document.getElementById('root');

render(
    <Provider store={store}>
        <Router history={browserHistory} routes={routes} />
    </Provider>,
    root
);
