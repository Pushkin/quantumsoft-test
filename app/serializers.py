from rest_framework import serializers
from app.models import DataNode


class DataNodeSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False)

    class Meta:
        model = DataNode
        fields = ('id', 'title', 'path', 'is_deleted')

    @classmethod
    def serialize_whole_tree(cls):
        return [
            dict(cls(node).data)
            for node in list(DataNode.objects.all().order_by('id',))
        ]
