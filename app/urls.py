from django.conf.urls import url
from app.views import GetDataNodeView, ApplyNodeChangesView, GetWholeTreeView, ResetView

urlpatterns = [
    url(r'^data_node/all/$', GetWholeTreeView.as_view(), name='data-node-all'),
    url(r'^reset/$', ResetView.as_view(), name='reset'),
    url(r'^data_node/(?P<id>[0-9]+)/$', GetDataNodeView.as_view(), name='data-node-get'),
    url(r'^apply_node_changes/$', ApplyNodeChangesView.as_view(), name='data-node-changes'),
]
