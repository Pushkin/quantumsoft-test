from rest_framework.response import Response
from rest_framework import status


def catch_request_errors_decorator(func):
    def handle(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except Exception as e:
            status_code = status.HTTP_400_BAD_REQUEST
            if hasattr(e, 'status_code'):
                status_code = e.status_code
            return Response(str(e), status=status_code)

    return handle


def get_nodes_initial_list_info():
    return [
        {'id': 0, 'title': 'root item', 'path': '0'},
        {'id': 1, 'title': 'item 1', 'path': '0.1'},
        {'id': 2, 'title': 'item 2', 'path': '0.2'},
        {'id': 3, 'title': 'item 3', 'path': '3'},
        {'id': 4, 'title': 'item 4', 'path': '3.4'},
        {'id': 5, 'title': 'item 5', 'path': '0.1.5'},
        {'id': 6, 'title': 'item 6', 'path': '0.1.6'},
        {'id': 7, 'title': 'item 7', 'path': '0.1.7'},
        {'id': 8, 'title': 'item 8', 'path': '0.1.6.8'},
        {'id': 9, 'title': 'item 9', 'path': '0.1.6.9'},
        {'id': 10, 'title': 'item 10', 'path': '0.1.6.9.10'},
        {'id': 11, 'title': 'item 11', 'path': '0.1.6.9.11'},
        {'id': 12, 'title': 'item 12', 'path': '12'},
        {'id': 13, 'title': 'item 13', 'path': '13'},
        {'id': 14, 'title': 'item 14', 'path': '12.14'},
    ]
