from django.db import models


# Materialized path pattern
class DataNode(models.Model):
    title = models.CharField(max_length=150, null=False)
    path = models.TextField(null=False, default='')
    is_deleted = models.BooleanField(null=False, default=False)

    @classmethod
    def get_branch(cls, branch_origin_path):
        return cls.objects.filter(path__contains=branch_origin_path)

    @classmethod
    def generate_new_item_id(cls):
        # - get last node id and increment it
        # - if no node is in the database, 0 will be set as id by default (-1 + 1)
        return str(getattr(DataNode.objects.last(), 'id', -1) + 1)

    @classmethod
    def item_exists(cls, **kwargs):
        try:
            cls.objects.get(**kwargs)
        except cls.DoesNotExist:
            return False

        return True
