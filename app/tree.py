import json
from operator import itemgetter
from django.db import transaction, connection
from django.db.utils import ProgrammingError
from app.serializers import DataNodeSerializer
from app.models import DataNode

# Values are given according to action priorities
ACTION_CREATE = 1
ACTION_UPDATE = 2
ACTION_DELETE = 3


class InvalidNodeDataException(Exception):
    pass


class DataNodeNotFound(Exception):
    pass


class TreeActionManager(object):
    def __init__(self, processed_nodes):
        self.action_queue = self._construct_action_queue(processed_nodes)
        self.new_nodes_temp_paths = []

    # @transaction.atomic
    def process_nodes(self):
        self.new_nodes_temp_paths = []
        new_nodes_assignments = {}

        for action in self.action_queue:
            if action['type'] == ACTION_CREATE:
                new_nodes_assignments.update(self._process_create_node(action['node'], new_nodes_assignments))

            elif action['type'] == ACTION_DELETE:
                self._process_delete_node(action['node'])

            else:
                self._process_update_node(action['node'])

        return new_nodes_assignments

    @staticmethod
    def _find_new_parent_path(parent_path, node_assignments):

        path_parts = parent_path.split('.')

        for index, part in enumerate(path_parts):
            part = int(part)

            if part < 0:
                if part not in node_assignments:
                    raise DataNodeNotFound('Wrong parent_path. Such node doesn\'t exist')

                path_parts[index] = str(node_assignments[part]['id'])

        return '.'.join(path_parts)

    @classmethod
    def _process_create_node(cls, node, node_assignments):
        # Generating path for a new element
        parent_path = '.'.join(node.path.split('.')[:-1])
        new_id = DataNode.generate_new_item_id()
        new_node_path = new_id

        if parent_path != '':
            if not DataNode.item_exists(path=parent_path):
                parent_path = cls._find_new_parent_path(parent_path, node_assignments)

            new_node_path = '{}.{}'.format(parent_path, new_node_path)

        old_id = node.id
        node.id = new_id
        node.path = new_node_path
        node.save()

        return {old_id: dict(DataNodeSerializer(node).data)}

    @classmethod
    def _process_update_node(cls, node):
        if not DataNode.item_exists(id=node.id):
            raise DataNodeNotFound('Unable to update non-existing node')

        node.save()

    @classmethod
    def _process_delete_node(cls, node):
        if not DataNode.item_exists(id=node.id):
            return

        # Applying all changes in the node before marking it as deleted
        node.save()

        query = '''
            UPDATE %s
            SET is_deleted = TRUE
            WHERE path like %s
        ''' % (DataNode._meta.db_table, "'{}%'".format(node.path))
        cls._execute_raw_query(query)

    def _construct_action_queue(self, processed_nodes):
        # Constructing action queue according to priority
        # Create-actions with the shortest paths come first

        all_actions = [self._construct_action_by_node_info(node) for node in processed_nodes];

        create_actions = sorted(
            list(filter(lambda node: node['type'] == ACTION_CREATE, all_actions)),
            key=lambda action: len(action['node'].path),
        )
        rest_actions = sorted(
            list(filter(lambda node: node['type'] != ACTION_CREATE, all_actions)),
            key=itemgetter('type')
        )

        return create_actions + rest_actions

    @classmethod
    def _construct_action_by_node_info(cls, node_info):
        node_model = cls._init_node_model(node_info)

        return {
            'type': cls._detect_action_by_node_info(node_model),
            'node': node_model
        }

    @staticmethod
    def _detect_action_by_node_info(node):
        # New nodes have negative id values because front-end never know last actual node id in the database
        result = ACTION_UPDATE

        if node.id < 0:
            result = ACTION_CREATE

        if node.is_deleted:
            result = ACTION_DELETE

        return result

    @staticmethod
    def _init_node_model(node_data):
        try:
            serialized_node = DataNodeSerializer(data=node_data)
            serialized_node.is_valid(raise_exception=True)
            return DataNode(**serialized_node.validated_data)
        except Exception as e:
            raise InvalidNodeDataException('Invalid node data: {}. \n Error: {}'.format(json.dumps(node_data), e))

    @staticmethod
    def _execute_raw_query(query_template):
        cursor = connection.cursor()
        cursor.execute(query_template.strip())

        try:
            return cursor.fetchall()
        except ProgrammingError:
            pass
