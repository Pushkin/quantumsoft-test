import json
from app.models import DataNode
from .base import PreparedBaseTestCase, BaseTestCase


class GetNodesTestCase(PreparedBaseTestCase):
    def test_get_node(self):
        correct_response = self._send_request(
            self.GET_DATA_NODE_URL,
            method=self.METHOD_GET,
            url_data={'id': self.nodes[5].id}
        )
        node_data = json.loads(correct_response.data)

        # Passing non-existing id
        incorrect_response = self._send_request(self.GET_DATA_NODE_URL, method=self.METHOD_GET, url_data={'id': 999})

        self.assertEqual(correct_response.status_code, 200)
        self.assertEqual(incorrect_response.status_code, 404)
        self.assertEqual(node_data['title'], self.nodes[5].title)

    def test_get_whole_tree(self):
        response = self._send_request(
            self.GET_WHOLE_TREE_URL,
            method=self.METHOD_GET,
        )

        nodes_data = json.loads(response.data)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(nodes_data), len(self.nodes))

        for node in nodes_data:
            self.assertEqual(
                node['title'],
                next(private_node.title for private_node in self.nodes if private_node.id == node['id'])
            )


class ResetNodesTestCase(BaseTestCase):
    def test_reset_nodes(self):
        response = self._send_request('reset', self.METHOD_GET)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(DataNode.objects.count(), 15)


class ApplyNodeChangesTestCase(PreparedBaseTestCase):
    def test_apply_node_changes(self):
        node_updated_title = 'Changed Title 1'
        new_node_title = 'New node 1'
        new_node_title_2 = 'New node 2'
        new_title_before_delete = 'New title before delete'

        request_data = [
            # Updating an existing node
            {'id': self.nodes[0].id, 'title': node_updated_title, 'path': self.nodes[0].path},
            # Removing an existing node
            {'id': self.nodes[7].id, 'title': new_title_before_delete, 'path': self.nodes[7].path, 'is_deleted': True},
            # Appending a new node to an existing node
            {'id': -1, 'title': new_node_title, 'path': self.nodes[0].path + '.-1'},
            # Appending a new node to a node which is gonna be deleted
            {'id': -2, 'title': new_node_title_2, 'path': self.nodes[7].path + '.-2'},
        ]

        response = self._send_request('data-node-changes', self.METHOD_POST, request_data=request_data)

        self.assertEqual(response.status_code, 200)

        # check if node name is changed
        self.assertEqual(DataNode.objects.get(id=self.nodes[0].id).title, node_updated_title)

        # check if deleting a one node caused deleting the whole branch
        for node in list(DataNode.get_branch(self.nodes[7].path)):
            self.assertEqual(node.is_deleted, True)

        # check if title is changed in deleting node
        self.assertEqual(DataNode.objects.get(id=self.nodes[7].id).title, new_title_before_delete)

        # check if new nodes are created
        self.assertEqual(DataNode.item_exists(title=new_node_title), True)
        self.assertEqual(DataNode.item_exists(title=new_node_title_2), True)

        # check if new nodes assignments are in response
        response_data = json.loads(response.data)
        self.assertEqual(len(response_data['new_nodes_assignments'].keys()), 2)

        self.assertTrue('-1' in response_data['new_nodes_assignments'].keys())
        self.assertEqual(response_data['new_nodes_assignments']['-1']['title'], new_node_title)

        self.assertTrue('-2' in response_data['new_nodes_assignments'].keys())
        self.assertEqual(response_data['new_nodes_assignments']['-2']['title'], new_node_title_2)

    def test_apply_non_existing_node_delete(self):
        request_data = [
            # Removing non-existing node
            {'id': 124534, 'title': 'Non existing node', 'path': '0.1.2.124534', 'is_deleted': True},
        ]

        response = self._send_request('data-node-changes', self.METHOD_POST, request_data=request_data)

        self.assertEqual(response.status_code, 404)

    def test_apply_non_existing_node_update(self):
        request_data = [
            # Updating non-existing node
            {'id': 124534, 'title': 'Non existing node', 'path': '0.1.2.124534'}
        ]

        response = self._send_request('data-node-changes', self.METHOD_POST, request_data=request_data)

        self.assertEqual(response.status_code, 404)

