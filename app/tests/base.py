import json
from django.test import TestCase, Client
from django.core.urlresolvers import reverse
from app.models import DataNode


class BaseTestCase(TestCase):
    TEST_CONTENT_TYPE = 'application/json'

    METHOD_GET = 'get'
    METHOD_POST = 'post'
    METHOD_PUT = 'put'
    METHOD_PATCH = 'patch'
    METHOD_DELETE = 'delete'

    CREATE_DATA_NODE_URL = 'data-node-create'
    GET_DATA_NODE_URL = 'data-node-get'
    GET_WHOLE_TREE_URL = 'data-node-all'

    def setUp(self):
        self.client = Client()

    def _send_request(self, reverse_url, method=METHOD_GET, request_data=None, url_data=None):
        url = reverse(reverse_url, kwargs=url_data)

        if request_data is not None:
            request_data = json.dumps(request_data)

        client_method = self.client.get

        if method == self.METHOD_POST:
            client_method = self.client.post
        elif method == self.METHOD_PUT:
            client_method = self.client.put
        elif method == self.METHOD_PATCH:
            client_method = self.client.patch
        elif method == self.METHOD_DELETE:
            client_method = self.client.delete

        return client_method(url, data=request_data, content_type=self.TEST_CONTENT_TYPE)


class PreparedBaseTestCase(BaseTestCase):
    def setUp(self):
        super(PreparedBaseTestCase, self).setUp()

        # Creating a simple example tree
        self.nodes = [
            DataNode.objects.create(title='root item', path='0'),
            DataNode.objects.create(title='item 1', path='0.1'),
            DataNode.objects.create(title='item 2', path='0.2'),
            DataNode.objects.create(title='item 3', path='1.3'),
            DataNode.objects.create(title='item 4', path='1.4'),
            DataNode.objects.create(title='item 5', path='2.5'),
            DataNode.objects.create(title='item 6', path='1.4.6'),
            DataNode.objects.create(title='item 7', path='1.4.7'),
            DataNode.objects.create(title='item 8', path='2.5.8'),
            DataNode.objects.create(title='item 9', path='2.5.9'),
            DataNode.objects.create(title='item 10', path='1.4.7.10'),
            DataNode.objects.create(title='item 11', path='1.4.7.11'),
            DataNode.objects.create(title='item 12', path='1.4.7.12'),
            DataNode.objects.create(title='item 13', path='1.4.7.11.13'),
            DataNode.objects.create(title='item 14', path='1.4.7.11.14'),
        ]

