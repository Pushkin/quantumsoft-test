import json
from django.core.exceptions import ObjectDoesNotExist
from rest_framework.views import APIView
from rest_framework import status
from rest_framework.response import Response
from rest_framework.renderers import JSONRenderer
from app.utils import catch_request_errors_decorator, get_nodes_initial_list_info
from app.serializers import DataNodeSerializer
from app.models import DataNode
from app.tree import TreeActionManager, DataNodeNotFound


class JSONResponse(Response):
    def __init__(self, data=None, status=None, template_name=None,
                 headers=None, exception=False, content_type=None):

        data = JSONRenderer().render(data)

        super(JSONResponse, self).__init__(
            data=data, status=status, template_name=template_name, headers=headers,
            exception=exception, content_type=content_type
        )


class ResetView(APIView):
    @catch_request_errors_decorator
    def get(self, request):
        DataNode.objects.all().delete()

        for node in get_nodes_initial_list_info():
            DataNode.objects.create(**node)

        return JSONResponse(status=status.HTTP_200_OK, data=DataNodeSerializer.serialize_whole_tree())


class GetWholeTreeView(APIView):
    @catch_request_errors_decorator
    def get(self, request):
        return JSONResponse(status=status.HTTP_200_OK, data=DataNodeSerializer.serialize_whole_tree())


class GetDataNodeView(APIView):
    @catch_request_errors_decorator
    def get(self, request, id=None):
        if id is None:
            raise Exception('No node ID specified')

        try:
            id = int(id)
        except ValueError:
            raise Exception('Unable to recognize ID')

        try:
            node = DataNode.objects.get(id=id)
        except ObjectDoesNotExist:
            return JSONResponse(
                status=status.HTTP_404_NOT_FOUND,
                data={'error_message': 'Unable to find node with given ID'}
            )

        if node.is_deleted:
            return JSONResponse(
                status=status.HTTP_423_LOCKED,
                data={'error_message': 'Node was deleted'}
            )

        return JSONResponse(status=status.HTTP_200_OK, data=DataNodeSerializer(node).data)


class ApplyNodeChangesView(APIView):
    @catch_request_errors_decorator
    def post(self, request):
        try:
            new_nodes_assignments = TreeActionManager(json.loads(request.body)).process_nodes()
        except DataNodeNotFound as e:
            return JSONResponse(status=status.HTTP_404_NOT_FOUND, data={'error_message': str(e)})

        return JSONResponse(status=status.HTTP_200_OK, data={'new_nodes_assignments': new_nodes_assignments})
