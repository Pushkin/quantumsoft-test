A test project for QuantumSoft fronend/full-stack position

The application provides tree items displaying and editing. It is divided to Database part and Cache part (conceptually, database is too large to be immediately downloaded into front-end application, though DB View exists on front-end page just for testing purposes)
!IMPORTANT The cache can only send requests to database to get a separate node or to apply all the changes made in the cache. Cache shouldn't make user misunderstand the real database state.

This project actually is a django application + react.js frontend application stored in the 'client' folder. 

Pre-requirements (should be installed on the machine):
- POSIX-compatible OS
- PostgreSQL 9.5 +
- Python3
- Virtualenv

Preparing Database:
1) Firts, we need to create a user with name "tree_app", password "123456" and login permissions. Createdb permission is also needed in order to run tests.

```
create user tree_app with login password '123456' createdb;
```

2) Then, we need to create database tree_app and set user tree_app as owner

```
create database tree_app owner tree_app;
```


Preparing Back-end application:
1) cd to the project folder
2) run ```./install.sh``` - It will create virtualenv, install all the packages and apply migrations
3) Now you can start the server
```
source env/bin/activate
python manage.py runserver
```
4) You can check the server is up by going to localhost:8000/api/data_node/all. You will see django-rest-framework dashboard and the request result where 15 JSON-serialized records should be returned. It also will mean that migrations were applied correctly.


Preparing Front-end application:
1) Being in the project folder already cd to client folder
2) Run ```npm i``` 
3) After that run ```npm run watch```
4) After everything is built up you can go to localhost:8001 and see front-end application.