# Pre-requirements:
# - Python3
# - Virtualenv

echo 'Creating virtual environment...'
virtualenv -p python3 env

echo 'Installing python dependencies...'
env/bin/pip install -r requirements.txt

echo 'Applying migrations...'
env/bin/python manage.py migrate